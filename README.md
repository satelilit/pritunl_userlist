# Pritunl_userlist

List all Pritunl's VPN user, including their email, groups and disabled/not status.

# Requirements
- `python 3`
- `pymongo`
- `python-dotenv`

# Usage
- Create .env file with values below:
  ```
  PRITUNL_AUTH_USER=
  PRITUNL_IP_ADDR=
  PRITUNL_AUTH_PASSWORD=
  ```
- run the script: `python3 pritunl_userlist.py`

# Example
```
 python3 pritunl_userlist.py
```
```
==============================================================================================================================
|name                                    email                           groups                                 disabled     |
==============================================================================================================================
|foo                                     foo@bar.net                     ['vpn']                                   False     |
|edi_supono                              parto@ngelawak.net              ['vpn']                                   False     |

```