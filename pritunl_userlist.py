#!/usr/bin/env python3
from pymongo import MongoClient
from dotenv import load_dotenv
import os

try:
    f = open(".env")
    load_dotenv(dotenv_path=".env")
except IOError:
    load_dotenv(dotenv_path=".env.example")

authUser = os.getenv('PRITUNL_AUTH_USER')
dbAddress = os.getenv('PRITUNL_IP_ADDR')
authPassword= os.getenv('PRITUNL_AUTH_PASSWORD')

dash = "=" *141

connURI = "mongodb://" + authUser + ":" + authPassword +"@"+ dbAddress + ":27017/?authSource=pritunl"

dbConn = MongoClient(connURI)
dbName = dbConn["pritunl"]
collection = dbName["users"]
result = collection.find({"type":"client"}).sort("groups",-1)

print(dash)
print("|"+"name".ljust(40)+"email".ljust(40)+"groups".ljust(50)+"disabled".ljust(9)+"|")
print(dash)
for result in result:
    print("|"+result['name'].ljust(40)+str(result['email']).ljust(40)+str(result['groups']).ljust(50)+str(result['disabled']).ljust(9)+"|")
print(dash)